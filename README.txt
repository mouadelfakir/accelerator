Steps to Training :

1. Download and install GIT and TortoiseGit (As Administrator) : 
	GIT : https://git-scm.com/downloads
	TortoiseGit : https://tortoisegit.org/download
	
2. Create an new account in bitbucket.org.

3. Create an new directory in your local machine and name it training_accelerator.
	
4. Open training_accelerator directory > right click > Git Bash Here.

5. Run the following Git commands one by one :
	git init
	git config --system core.longpaths true
	git config --system http.sslverify false
	git remote add origin https://mouadfkr@bitbucket.org/mouadfkr/accelerator.git
	git pull origin master

6. Now you dispose of a clone of the repository and you are ready to start developing.

7. Now go to training_accelerator/accelerator and past the config folder here (get config folder from the last accelerator training).

8. Then got to training_accelerator/accelerator/bin and past all other extensions here except custom (get the extensions from the last accelerator training).

9. Run Ant clean all

10. Start hybris server

11. Run initialize system from HAC.

12. And check storefront http://localhost:9001/store?site=hybris
